# client.py

import socket

MAX_MURAL = 16
tam_mural = False
MAX_MSG = 256
tam_msg = False

UDP_IP = "127.0.0.1"  	   # Endereco IP do Servidor
UDP_PORT = 5005            # Porta que o Servidor

udp = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)		# Cria Socket UDP

dest = (UDP_IP, UDP_PORT)

while True:

	print 'Selecione a funcionalidade desejada: \n'
	print '1 - Criar novo mural ou enviar novo recado\n'
	print '2 - Ler recados do mural\n'
	opcao = raw_input()

	if opcao == '1':
		print 'Digite o mural e o recado a ser postado...\n'

		print 'Mural:'
		mural = raw_input()
		if len(mural) <= MAX_MURAL:
			tam_mural = True

		print 'Recado:'
		msg = raw_input()
		if len(msg) <= MAX_MSG:
			tam_msg = True
		
		if tam_mural and tam_msg:
			data = 'POST' + ';' + mural + ';' + msg
   			udp.sendto (data, dest)
		else:
			tam_msg = False
			tam_mural = False
			print 'Tamanhos de mural e recado excedidos.\n'

	elif opcao == '2':
		print 'Informe o mural e o indice do recado a ser consultado...\n'

		print 'Mural:'
		mural = raw_input()
		if len(mural) <= MAX_MURAL:
			tam_mural = True

		print 'Indice:'
		msg = raw_input()
		if len(msg) <= MAX_MSG:
			tam_msg = True

		if tam_mural and tam_msg:
			data = 'GET' + ';' + mural + ';' + msg
   			udp.sendto (data, dest)
		else:
			tam_msg = False
			tam_mural = False
			print 'Tamanhos de mural e indice excedidos.\n'			

	if tam_mural and tam_msg:
		data, endereco = udp.recvfrom(1024)  # tamanho do buffer: 1024 bytes
		print "Mensagem de confirmacao recebida do servidor:", "["+data+"] \n"
	else:		
		print "Operacao cancelada.\n"

	print '============================================================= \n'

udp.close()
