Descrição:

Projeto de protocolo de comunicação de um Mural de mensagens.
Um protocolo para mensagens curtas entre usuários foi imaginado para funcionar como um mural de recados. Um usuário pode acessar o servidor de murais, e lá deixar uma ou mais mensagens em um determinado mural. Murais podem ser criados, sendo identificados por nomes arbitrários. Cada mensagem armazenada em um mural é etiquetada com a data e horário de publicação, e também sua duração (o autor da mensagem não é identificado pelo protocolo). Mensagens de um mural são ordenadas pela data e horário de publicação, e são removidas automaticamente ao expirarem. Usuários podem também acessar o servidor de murais e obterem as mensagens de um mural. Para isso, deve-se conhecer o nome do mural que se deseja acessar, pois o protocolo não possibilita listar os murais existentes. As mensagens são obtidas na ordem em que estão dispostas no mural, porém pode-se filtrá-las por intervalos de data e horário de publicação.

- Especificação do serviço provido pelo protocolo:

    - protocolo de aplicação do tipo cliente-servidor
    - protocolo orientado a mensagens
    - modo não-conectado
    - publicação de mensagens anônimas, curtas e com duração definida, em repositórios denominados murais ou canais
    - obtenção de mensagens com anonimato, a partir da identificação do mural ou canal que as contém 
    
- Considerações sobre o ambiente de execução do protocolo:

    - canal de comunicação UDP
    - protocolo executado em plataforma com memória ilimitada e capacidade de processamento elevada
    - taxa de transmissão variável, podendo ser baixa
    
- Definição do vocabulário e codificação de mensagens a ser adotada: 

    - GET: obter mensagem
    
        [
        mural: string 16 caracteres ASCII
        indice: numero da mensagem, inteiro de 8 bits
        ]
        
    - POST: publicar mensagem
    
        [
        mural: string 16 caracteres ASCII
        mensagem: string de até 256 caracteres ASCII
        ]
        
    - Resposta:
    
        [
        status: inteiro de 8 bits com código de resposta
        len: quantidade de bytes de conteúdo da mensagem retornada, inteiro de 8 bits
        timestamp: o valor da data e horário na forma de um inteiro longo de 64 bits big-endian
        conteudo: string de len bytes (sel len > 0) 
        ]
        
- Descrição do comportamento do protocolo:


- Implementação do protótipo do protocolo:

    - Servidor de murais
    - Uma aplicação cliente 

