# server.py

import socket
import string
import struct
import time
import calendar
from threading import Timer

class meu_mural:
	def __init__(self, name, message, timestamp):
		self.nome = name
		self.mensagens = []
		self.mensagens.append(message)
		self.timers = []
		self.timers.append(timestamp)
		self.set_timer()

	def set_timer(self):
		self.timeout = Timer(120.0, self.timer_handler).start() # Start timer and after 10 seconds calls timer_handler

	def timer_handler(self):
		# TIMEOUT => limpa itens respectivos da mensagem com timeout do mural
		self.mensagens.pop(0) #exclui mensagem do mural
		self.timers.pop(0)   #exclui timestamp da mensagem

#=============================================================================

def pega_ts_s(): # retorna timestamp em segundos

	ts_s = time.gmtime()
	#print ts_s

	return ts_s

def pega_ts_d(): # retorna timestamp em formato data/hora

	ts_s = pega_ts_s()

	#ts_d = time.strftime("%x %X", ts_s)
	ts_d = time.strftime("%d-%m-%Y %H:%M:%S", ts_s)

	#print ts_d

	return ts_d

def converte_timestamp(ts_s): # converte ts_s para inteiro longo de 64 bits big-endian

	timestruct = struct.pack('>L', ts_s)
	#print timestruct

	return timestruct

#=============================================================================
UDP_IP = ''
UDP_PORT = 5005         # Porta que o Servidor esta

udp = socket.socket(socket.AF_INET, socket.SOCK_DGRAM) # Cria Socket UDP

orig = (UDP_IP, UDP_PORT)

udp.bind(orig)

murais = [] 		# lista de murais do servidor
tem_mural = False
tstamp = 0

while True:
	data, endereco = udp.recvfrom(1024)  # tamanho do buffer: 1024 bytes
	#print "dado recebido:", data

	campos = string.split(data, ';')	# separa cada campo da mensagem ( por exemplo: POST, mural, mensagem)

	if campos[0] == 'POST': # se operacao recebida for POST, entao inicia logica para criar murais
		if len(murais) > 0: # se a lista de murais ja possui no min. um mural criado
			for mural in murais:
				if (campos[1] in mural.nome):
					tem_mural = True
					mural.mensagens.append(campos[2]) # adiciona nova mensagem na lista de mensagens do mural ja existente
					mural.timers.append(pega_ts_d()) # calcula timestamp exato do recebimento da mensagem
					mural.set_timer()
					break

			if tem_mural == False:
				tstamp = pega_ts_d() #  calcula timestamp exato do recebimento da mensagem
				murais.append(meu_mural(campos[1], campos[2], tstamp)) # senao adiciona novo mural para a lista de murais

			tem_mural = False
	
		else:
			tstamp = pega_ts_d() #  calcula timestamp exato do recebimento da mensagem
			murais.append(meu_mural(campos[1], campos[2], tstamp)) # adiciona novo mural para a lista de murais

		# Responde o cliente com sucesso na operacao de POST
		resposta = '1' + ';' + '0'
		udp.sendto(resposta, endereco)

		for item in murais:
			print item.nome
			print item.mensagens
			print item.timers

		print '============================================================='

	elif campos[0] == 'GET':

		# faz consulta no mural com os parametros da mensagem recebida:
		for mural in murais:
			if mural.nome == campos[1]: # se existir mural com mesmo nome da consulta
				tam = len(mural.mensagens)
				if tam > 0 and tam >= int(campos[2]): # se existir mensagem com mesmo indice da consulta
			
					# Responde o cliente com sucesso na operacao de GET
					resposta = '1' + ';' + str(len(mural.mensagens[int(campos[2])-1])) + ';' + mural.timers[int(campos[2])-1] + ';' + mural.mensagens[int(campos[2])-1]
					udp.sendto(resposta, endereco)
		
		# Responde o cliente com fracasso na operacao de GET
		resposta = '0' + ';' + '0'
		udp.sendto(resposta, endereco)

udp.close()
